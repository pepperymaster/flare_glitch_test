import 'package:flutter/material.dart';
import 'package:flare_flutter/flare_actor.dart';


const String B_APARECE = "Appear";
const String B_DESAPARECE = "Disappear";
const String B_OCULTO = "Hide";
const String B_CORRECTO_ON = "Correct_on";
const String B_CORRECTO_OFF = "Correct_off";
const String B_ERROR_ON = "Wrong_on";
const String B_ERROR_OFF = "Wrong_off";

const String MSG_CORRECT = "CORRECT";
const String MSG_DEFAULT = "Press de Green buttom";

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String smallAnimation = B_APARECE;
  bool isAnimatingBG = false;
  bool isStarting = false;
  bool isFirstStart = true;
  bool isFirstContainer = false;
  bool isSecondContainer = false;
  bool isThirdContainer = false;
  bool isSecondVisible = false;
  bool isColoresSet = false;
  bool isCuadrantesSet = false;
  bool isCorrectSelected = false;
  bool guardando = true;
  String activeAnimation;
  String msg = MSG_DEFAULT;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: Center(

        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Text(
                msg,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Expanded(
              child: Container(
                child: FlareActor(
                  "assets/blue.flr",
                  shouldClip: false,
                  fit: BoxFit.contain,
                  animation: smallAnimation,
                  callback: (string){
                    isAnimatingBG = false;
                    debugPrint(string);
                    if(!isCorrectSelected){
                      isCorrectSelected = !isCorrectSelected;
                      if(_canAnimateTo(B_DESAPARECE)){
                        isAnimatingBG = true;
                        setState(() {
                          smallAnimation = B_DESAPARECE;
                        });
                      }
                    }

                    if(smallAnimation != B_APARECE){
                      if(_canAnimateTo(B_APARECE)) {
                        isAnimatingBG = true;
                        setState(() {
                          smallAnimation = B_APARECE;
                        });
                      }
                    }

                  },),
              ),
            ),
            Expanded(
              child: _setSpecificContainer(this),
            ),
          ],
        ),
      ),
    );
  }

  bool _canAnimateTo(String anim) {
    return (!isAnimatingBG && smallAnimation != anim);
  }

  Widget _setSpecificContainer ( _MyHomePageState parent){

    return ContainerOne(
      xA: 0.1,
      xB: 0.1,
      yA: 0.1,
      yB: 0.1,
      xC: 0.1,
      yC: 0.1,
      buttonA: new AnimatedButton(isCorrect: false, strAnimation: "assets/yellow.flr",parent: parent,),
      buttonB: new AnimatedButton(isCorrect: true, strAnimation: "assets/green.flr",parent: parent,),
      buttonC: new AnimatedButton(isCorrect: false, strAnimation: "assets/red.flr",parent: parent,),
    );

  }

}

class ContainerOne extends StatelessWidget{
  final double xA;
  final double yA;
  final double xB;
  final double yB;
  final double xC;
  final double yC;
  final Widget buttonA;
  final Widget buttonB;
  final Widget buttonC;

  ContainerOne({@required this.buttonA, @required this.xA, @required this.yA, @required this.buttonB, @required this.xB, @required this.yB,
  @required this.xC, @required this.yC, @required this.buttonC});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                alignment: Alignment(xA, yA),
                child: buttonA,
              ),
            ),
            Expanded(
              child: Container(
                alignment: Alignment(xB, yB),
                child: buttonB,
              ),
            ),
            Expanded(
              child: Container(
                alignment: Alignment(xC, yC),
                child: buttonC,
              ),
            ),
          ],
        )
    );
  }
}

class AnimatedButton extends StatefulWidget{
  final bool isCorrect;
  final String strAnimation;
  final _MyHomePageState parent;
  AnimatedButton({Key key, @required this.isCorrect, @required this.strAnimation, @required this.parent}):super(key: key);

  @override
  State<StatefulWidget> createState() => new ColoredButton(isCorrect: isCorrect, strAnimation: strAnimation, parent: parent);
}

class ColoredButton extends State<AnimatedButton>{
  bool isCorrect = false;
  String strAnimation;
  _MyHomePageState parent;
  ColoredButton({ @required this.isCorrect, @required this.strAnimation, @required this.parent});
  String buttonAnimation = B_APARECE;
  bool isAnimating = false;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 80.0,
        width: 80.0,
        child: GestureDetector(
          child: FlareActor(
            strAnimation,
            shouldClip: false,
            fit: BoxFit.contain,
            animation: buttonAnimation,
            callback: (string){
              isAnimating = false;
              debugPrint(string);
              if(buttonAnimation == B_CORRECTO_ON){
                this.parent.setState((){
                  this.parent.isCorrectSelected = true;
                  this.parent.isSecondVisible = false;
                });
              }
              _animate(false);
            },),
          onTap: (){
            _animate(true);
              if(isCorrect){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>MyApp()));
              }
          },
        )
    );
  }

  bool _canAnimateTo(String anim) {
    return (!isAnimating && buttonAnimation != anim);
  }

  void _animate(bool click){
    if(click){
      if(isCorrect){
        if(_canAnimateTo(B_CORRECTO_ON)){
          isAnimating = true;
          setState(() {
            buttonAnimation = B_CORRECTO_ON;
            this.parent.setState(() {
              this.parent.msg = MSG_CORRECT;
              this.parent.activeAnimation = buttonAnimation;
            });
          });

        }
      }else{
        if(_canAnimateTo(B_ERROR_ON)){
          isAnimating = true;
          setState(() {
            buttonAnimation = B_ERROR_ON;
            this.parent.setState(() {
              this.parent.msg = "ERROR";
              this.parent.activeAnimation = buttonAnimation;
            });
          });
        }
      }
    }else{
      switch(buttonAnimation){
        case B_OCULTO:
          if(_canAnimateTo(B_APARECE)){
            isAnimating = true;
            setState((){
              buttonAnimation = B_APARECE;
              this.parent.setState(() {
                this.parent.msg = MSG_DEFAULT;
                this.parent.activeAnimation = buttonAnimation;
              });
            });
          }
          break;
        case B_CORRECTO_ON:
          if(_canAnimateTo(B_CORRECTO_OFF)){
            isAnimating = true;
            this.parent.setState((){
              buttonAnimation = B_CORRECTO_OFF;
              this.parent.setState(() {
                this.parent.msg = MSG_DEFAULT;
                this.parent.activeAnimation = buttonAnimation;
              });
            });
          }
          break;
        case B_ERROR_ON:
          if(_canAnimateTo(B_ERROR_OFF)){
            isAnimating = true;
            setState((){
              buttonAnimation = B_ERROR_OFF;
              this.parent.setState(() {
                this.parent.msg = MSG_DEFAULT;
                this.parent.activeAnimation = buttonAnimation;
              });
            });
          }
          break;
      }
    }
  }
}
